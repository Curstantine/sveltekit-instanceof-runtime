import { ActionFailure } from "@sveltejs/kit";
import { failChainedEmailUsernamePassword } from "../utils/validator";

/** @type {import('./$types').Actions} */
export const actions = {
	default: async (event) => {
		const { request } = event;

		const data = await request.formData();
		const validated = failChainedEmailUsernamePassword(data);
		console.log(validated);
		if (validated instanceof ActionFailure) return validated;

		return {
			body: "Success!"
		};
	}
};
