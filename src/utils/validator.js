import { ActionFailure, fail } from "@sveltejs/kit";

/**
 * @param {string} email
 * @returns {boolean}
 */
export const validateEmail = (email) => {
	return email.match(/^[^@]+@[^@]+\.[^@]+$/) !== null;
};

/**
 * @param {string} password
 * @returns {boolean}
 */
export const validatePassword = (password) => {
	return password.length >= 8;
};

/**
 * @param {string} username
 * @returns {boolean}
 */
export const validateUsername = (username) => {
	return username.match(/^[a-zA-Z0-9]+$/) !== null;
};

/**
 * @param {Record<string, string | FormDataEntryValue | null>} fields
 * @returns {string[]}
 */
export const validateNonEmptyFields = (fields) => {
	const missing = [];
	for (const [key, value] of Object.entries(fields)) {
		if (value == null || value.length === 0) missing.push(key);
	}

	return missing;
};

/**
 *
 * @param {FormData} formData
 * @returns {ActionFailure<{ missing?: string[], error?: string }> | { email: string, password: string, username: string }}
 */
export const failChainedEmailUsernamePassword = (formData) => {
	const email = formData.get("email")?.toString() ?? null;
	const username = formData.get("username")?.toString() ?? null;
	const password = formData.get("password")?.toString() ?? null;

	const missing = validateNonEmptyFields({ email, username, password });
	if (missing.length > 0) return fail(422, { missing });

	// @ts-expect-error -- We already checked for null values.
	if (validateEmail(email) === false) {
		return fail(422, { error: "Invalid email" });
	}

	// @ts-expect-error -- We already checked for null values.
	if (validateUsername(username) === false) {
		return fail(422, { error: "Invalid username" });
	}

	// @ts-expect-error -- We already checked for null values.
	if (validatePassword(password) === false) {
		return fail(422, { error: "Invalid password" });
	}

	return {
		// @ts-expect-error -- We already checked for null values.
		email,
		// @ts-expect-error -- We already checked for null values.
		username,
		// @ts-expect-error -- We already checked for null values.
		password
	};
};
